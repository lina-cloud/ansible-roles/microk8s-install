microk8s install
=========

Installs microk8s via snap on ubuntu

Requirements
------------

None

Role Variables
--------------

`microk8s_api_external_domain` is optional takes the fqdn for the api gateway cert

Dependencies
------------
None

----------------

This role is only available via this git repo 

    - hosts: servers
      roles:
         - role: microk8s-install,
           vars:
             microk8s_api_external_domain: "api.exsample.com"

License
-------

Mozilla Public License 2.0
